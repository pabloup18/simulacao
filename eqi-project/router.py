from flask import Flask, request
app = Flask("Simulacao_investimento")
@app.route("/simulacao", methods=["POST"])
def SimulaInvestimento(response):
    header = response.headers
    header['Access-Control-Allow-Origin', '*']
    header['Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS']
    header['Access-Control-Allow-Headers', 'Content-Type, x-access-token']
    body = request.get_json(force=True)
    # bloco de if's para checar se esta faltando algum dado necessario para realizar uma simulação
    if ("nome" not in body):
        return CreateResponse(400, {'resultado': 'okay'})
    if ("email" not in body):
        return CreateResponse(400, "O parametro email é obrigátorio")
    if ("telefone" not in body):
        return CreateResponse(400, "O parametro telefone é obrigátorio")
    if ("quantidade" not in body):
        return CreateResponse(400, "O parametro quantidade é obrigátorio")
    if ("tempo_investimento" not in body):
        return CreateResponse(400, "O parametro tempo_investimento é obrigátorio")

    # Valor inicial aplicado
    valor_do_investimento = int(body['quantidade'])

    # Rentabilidade mensagem, em %
    rentabilidade_mensal_poupanca = float(0.11)

    # Transformando a porcentagem em valor numérico
    rentabilidade_mensal_poupanca = rentabilidade_mensal_poupanca / 100

    # Tempo de investimento
    tempo_investimento = int(body['tempo_investimento'])

    valor_final_poupança = valor_do_investimento * (1+0.0011)**tempo_investimento
    valor_final_cdb = valor_do_investimento * (1+0.0022)**tempo_investimento
    valor_final_cdb_pos_fixado = valor_do_investimento * (1+0.0022)**tempo_investimento

    return {"valor final após "+str(tempo_investimento)+" meses na poupança(2020)": valor_final_poupança,
            "valor final após "+str(tempo_investimento)+" meses com taxas CDI (2020)": valor_final_cdb,
            "valor final após "+str(tempo_investimento)+" meses com taxas CDI (2020)": valor_final_cdb_pos_fixado
            }

def CreateResponse(status, mensagem, nome_conteudo=False, conteudo=False):
    response = {}
    response['status'] = status
    response['mensagem'] = mensagem
    
    if (nome_conteudo and conteudo):
        response[nome_conteudo]=conteudo
    
    return response
app.run()

